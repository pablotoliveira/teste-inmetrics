# Aqui é configuardo para ser tirado um print no final de cada teste
After do |scenario|
  log("==> Scenario '#{scenario.name}' finished")
  take_screenshot(scenario) if SCREENSHOT == 'all' || (SCREENSHOT == 'errors' && scenario.failed?)
  Capybara.current_session.driver.quit
end

# Método para tirar print da tela e salvar em "report/screenshots/" no formato PNG
def take_screenshot(scenario)
  screenshot_path = '././report/screenshots/' + "#{scenario.name}.png"
  page.save_screenshot(screenshot_path)
  attach(screenshot_path, 'image/png')
end
