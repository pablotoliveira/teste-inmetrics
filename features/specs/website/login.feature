#language: pt
# Feature referente aos testes de front de Login
# Tags utilizadas para rodar os testes separadamente
@login
Funcionalidade: Login
  - Eu como usuário, quero fazer login no sistema

  # Nesse caso eu separei os testes entre componente (Teste um componente separado)
  # E teste e2e (Teste o cadastro, edição e exclusão em um teste só)
  @componente @teste
  Cenário: Efetuar o login com sucesso
    Dado que acesso o website
    Quando preencho o usuario e a senha
    E clico em login
    Entao o usuario devera ser logado com sucesso

  @e2e
  Cenário: Efetuar o login com sucesso apos o cadastro
    Dado que acesso o website
    E cadastre um usuario pelo "link_central"
    Quando preencho o usuario e a senha cadastrado 
    E clico em login
    Entao o usuario devera ser logado com sucesso
