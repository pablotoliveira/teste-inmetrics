#language: pt
# Feature referente aos testes de front de funcionário
# Tags utilizadas para rodar os testes separadamente
@funcionario
Funcionalidade: Funcionario
  - Eu como usuário, quero cadastrar, alterar ou excluir um funcionario

  # Nesse caso eu separei os testes entre componente (Teste um componente separado)
  # E teste e2e (Teste o cadastro, edição e exclusão em um teste só)
  @componente
  Cenário: Cadastrar um funcionario
    Dado que eu esteja logado
    E acesso a tela de novo funcionario
    Quando preencho todos os campos de funcionario
    E clico em enviar
    Entao o funcionario devera ser cadastrado com sucesso

  @componente
  Cenário: Editar um funcionario
    Dado que eu esteja logado
    E abro o funcionario "Funcionario Tester Editar" para edicao
    Quando preencho todos os campos de funcionario
    E clico em enviar
    Entao o funcionario devera ser editado com sucesso

  @componente
  Cenário: Deletar um funcionario
    Dado que eu esteja logado
    E pesquiso o funcionario "Funcionario Tester Deletar"
    Quando deleto o funcionario
    Entao o funcionario devera ser deletado com sucesso

  @e2e
  Cenário: Cadastrar, editar e deletar um funcionario
    Dado que eu esteja logado
    E tenha um funcionario cadastrado "Funcionario CRUD"
    E abro o funcionario "Funcionario CRUD" para edicao
    Quando preencho todos os campos de funcionario
    E clico em enviar
    Entao o funcionario devera ser editado com sucesso
    Quando altero o nome do funcionario para "Funcionario CRUD"
    E pesquiso o funcionario editado "Funcionario CRUD"
    E deleto o funcionario
    Entao o funcionario devera ser deletado com sucesso