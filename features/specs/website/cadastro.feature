#language: pt
# Feature referente aos testes de front de Cadastro
# Tags utilizadas para rodar os testes separadamente
@cadastro @e2e
Funcionalidade: Cadastro
  - Eu como novo usuário, quero me cadastrar

  Cenário: Efetuar o cadastro com sucesso usando o link central
    Dado que acesso o website
    E acesso o cadastro de usuário pelo "link_central"
    Quando preencho os campos obrigatórios
    E clico em cadastrar
    Entao o usuario devera ser cadastrado
