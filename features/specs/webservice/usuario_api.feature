#language: pt
# Feature referente aos testes da API de usuário usando a collection "empregado-controller"
# Tags utilizadas para rodar os testes separadamente
@api_usuario @integracao
Funcionalidade: Usuarios
  - Eu como usuário, quero cadastrar, alterar, listar um ou mais ususarios

  Cenário: Cadastrar um usuario
    Dado que eu cadastre um usuario via POST
    Entao o usuario devera ter sido cadastrado com sucesso via POST

  Cenário: Listar todos usuarios cadastrados
    Dado que eu faça um GET no endpoint de todos usuarios
    Entao sera retornado todos os usuarios cadastrados

  Cenário: Listar usuario cadastrado
    Dado que eu faça um GET no endpoint de usuarios "2078"
    Entao sera retornado o usuario "2078" cadastrado

  Cenário: Alterar um usuario
    Dado que eu altere um usuario "2078" via PUT
    Entao o usuario "2078" devera ter sido alterado com sucesso via PUT