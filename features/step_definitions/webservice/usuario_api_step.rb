# Aqui é efetuado o cadastro do usuario via POST pela API
# Pego o header e o body de outra classe e envio um post para efetuar o cadastro
Given('que eu cadastre um usuario via POST') do
  @head_post_usuario = commons_api.pega_header
  @body_post_usuario = commons_api.pega_body_usuario.to_json

  @post_usuario = HTTParty.post(BASE_URI+"/empregado/cadastrar", basic_auth: AUTH, body: @body_post_usuario, headers: @head_post_usuario)
end

# Aqui é efetuado um filtro via GET pela API de todos usuarios cadastrados
Given('que eu faça um GET no endpoint de todos usuarios') do
  @get_usuarios = HTTParty.get(BASE_URI+"/empregado/list_all", basic_auth: AUTH)
end

# Aqui é efetuado um filtro via GET pela API de somente um usuário passado pelo Caso de Teste
Given('que eu faça um GET no endpoint de usuarios {string}') do |empregadoId|
  @get_usuario = HTTParty.get(BASE_URI+"/empregado/list/"+empregadoId, basic_auth: AUTH)
end

# Aqui é efetuado a edição de um usuario via PUT pela API
# Pego o header e o body de outra classe e envio um post para efetuar o cadastro
Given('que eu altere um usuario {string} via PUT') do |empregadoId|
  @head_put_usuario = commons_api.pega_header
  @body_put_usuario = commons_api.pega_body_usuario.to_json
  
  @put_usuario = HTTParty.put(BASE_URI+"/empregado/alterar/"+empregadoId, basic_auth: AUTH, body: @body_put_usuario, headers: @head_put_usuario)
end

# Aqui é validado se o filtro de todos os usuários cadastrados é feito com sucesso
Then('sera retornado todos os usuarios cadastrados') do
  expect(@get_usuarios.code).to eq 200
end

# Aqui é validado se o filtro de um usuário cadastrado é feito com sucesso
# Valido inclusive se o id do usuário é retornado pela API
Then('sera retornado o usuario {string} cadastrado') do |empregadoId|
  expect(@get_usuario.code).to eq 202
  expect(@get_usuario.body).to have_text '"empregadoId":'+empregadoId
end

# Aqui é validado se o usuário é cadastrado com sucesso
Then('o usuario devera ter sido cadastrado com sucesso via POST') do
  expect(@post_usuario.code).to eq 202
end

# Aqui é validado se o usuário é editado com sucesso
Then('o usuario {string} devera ter sido alterado com sucesso via PUT') do |empregadoId|
  expect(@put_usuario.code).to eq 202
end
