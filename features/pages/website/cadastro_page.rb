# Classe para métodos da funcionalidade de Cadastro de usuários
# Essa classe extende do SitePrism::Page para poder mapear os elementos e 
# deixa-los mais facil para ser reutilizados
class CadastroPage < SitePrism::Page

  # Elementos mapeados
  element :cadastre_link_central,     '.txt2'
  element :cadastro_usuario,          'input[name="username"]'
  element :cadastro_senha,            'input[name="pass"]'
  element :cadastro_confirmar_senha,  'input[name="confirmpass"]'
  element :cadastrar_btn,             '.login100-form-btn'

  # Método para acessar a tela de cadastro
  # A tela de cadastro pode ser acessada por 2 links ou digitando a URL 
  # Por isso eu criei o case, para poder adicionar as outras duas opções de acesso
  def acessar_tela_cadastro(tp_link)
    case tp_link
    when 'link_central'
      cadastre_link_central.click
    end
  end

  # Método para preencher os dados de cadastro
  def preencher_cadastro(usuario, senha)
    wait_until_cadastro_usuario_visible
    cadastro_usuario.set usuario
    cadastro_senha.set senha
    cadastro_confirmar_senha.set senha
  end

  # Método para efetuar o cadastro
  def cadastrar
    cadastrar_btn.click 
  end
end
