# Classe para métodos da funcionalidade referente ao login
# Essa classe extende do SitePrism::Page para poder mapear os elementos e 
# deixa-los mais facil para ser reutilizados
class LoginPage < SitePrism::Page

  # Elementos mapeados
  # Usei o elements para mapear um elemento que retorna mais de um item
  # Usei o element para mapear um elemento unico
  element :titulo_principal,     '.login100-form-title'
  element :login_usuario,        'input[name="username"]'
  element :login_senha,          'input[name="pass"]'
  element :login_btn,            '.login100-form-btn'
  elements :navbar_links,        'li.nav-item'

  # Método para preencher os dados de login
  def preencher_login(usuario, senha)
    login_usuario.set usuario
    login_senha.set senha
  end

  # Método para efetuar o login
  def fazer_login
    login_btn.click
  end
  
end
