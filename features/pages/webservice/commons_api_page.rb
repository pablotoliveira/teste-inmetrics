# Classe para métodos genéricos para a API
class CommonsApiPage
  include HTTParty #Incluindo a GEM HTTParty na classe

  #Método para configurar o cabeçalho da API
  def pega_header
    {
      'Content-Type' => 'application/json',
      'accept' => '*/*'
    }
  end

  #Método para configurar valores randomicos, usando o Faker para o body do usuario
  def pega_body_usuario
    {
      admissao: Date.today.strftime('%d/%m/%Y'),
      cargo: Faker::Name.first_name+Faker::Name.last_name,
      comissao: '0,00',
      cpf: Faker::CPF.pretty,
      departamentoId: 1,
      nome: Faker::Name.first_name+Faker::Name.last_name,
      salario: Faker::Number.between(from: 1, to: 10).to_s+'.000,00',
      sexo: Faker::Gender.short_binary_type,
      tipoContratacao: 'clt'
    }
  end

end