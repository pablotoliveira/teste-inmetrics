# desafio-inmetrics
Testes automatizados referente a um desafio de website e webservice.

### Dependência ###

```
  ruby 2.6.3
```

### Instrucoes para instalacao ###

#### Instalar o bundler
Para começar, instale o bundler:

```
  gem install bundler
```

#### Instalar as gems do projeto
Agora, atualize os pacotes necessários para executar o projeto:

```
  bundle install
```

#### Instalar o ChromeDriver

* https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver


### Instrucoes para executar o teste ###
O projeto usa o Cucumber para executar os testes. Você pode usar o seguinte comando para executar:

```
  cucumber -t @tag
```

Caso queira rodar os testes em modo headless, execute o seguinte comando:

```
  cucumber -t @tag -p chrome_headless
```


### TAGS

* e2e = Roda os testes da camada end to end
* componente = Roda os testes da camada de componente
* cadastro = Roda toda a funcionalidade de cadastro
* funcionario = Roda toda a funcionalidade de funcionario
* login = Roda toda a funcionalidade de login
* api_usuario = Roda toda a funcionalidade de usuário via API
* integracao = Roda todos os testes de integração