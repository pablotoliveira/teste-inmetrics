#! Configuração para rodar o projeto no Jenkins
pipeline {
    echo "Rodando com parâmetros: " +
            "\n    TAGS: ${TAGS} " +
            "\n    BROWSER: ${BROWSER} "
    agent any
    stages {
        stage('Build') {
            steps {
                sh 'gem install bundler'
                sh 'bundle install'
            }
        }
        stage('Test') {
            steps {
                sh "cucumber -t ${TAGS} -p ${BROWSER}"
            }
        }
    }
    post {
        success {
            echo 'Testes executados com sucesso'
        }
        failure {
            echo 'Testes executados com falha'
        }
    }
}